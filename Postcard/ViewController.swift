//
//  ViewController.swift
//  Postcard
//
//  Created by Neil on 27/10/2014.
//  Copyright (c) 2014 Coupaman. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var enterNameTextField: UITextField!
    @IBOutlet weak var enterMessageTextField: UITextField!
    @IBOutlet weak var mailButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func sendMailButtonPressed(sender: UIButton) {
        messageLabel.text = enterMessageTextField.text
        nameLabel.text = enterNameTextField.text

        messageLabel.textColor = UIColor.redColor()
        nameLabel.textColor = UIColor.blueColor()
        //messageLabel.textColor = UIColor(red:1.0, green:0.5, blue:0.5, alpha:0.9)

        enterMessageTextField.text = ""
        enterNameTextField.text = ""
        //enterMessageTextField.resignFirstResponder()
        //enterNameTextField.resignFirstResponder()
        self.view.endEditing(true)
        
        mailButton.setTitle("Mail Sent", forState: UIControlState.Normal)

        messageLabel.hidden = false
        nameLabel.hidden = false
    }

}

